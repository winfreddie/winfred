import 'package:flutter/material.dart';
import 'body.dart';

void main() {
  runApp(MyTrial());
}

class MyTrial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Happy holidays"),
          backgroundColor: Colors.amberAccent,
        ),
        body: Body(),
      ),
    );
  }
}
